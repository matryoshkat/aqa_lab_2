import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LabTest {
    WebDriver _driver;

    @Before
    public void Before() {
        _driver = new ChromeDriver();
    }


    @Test
    public void GooglePageTest() {
        String title;
        boolean isLogoPresent;
        boolean isSearchBarPresent;
        boolean isSearchButtonPresent;
        boolean isGmailLinkPresent;
        try {
            _driver.get("https://google.com/");
            title = _driver.getTitle();
            isLogoPresent = _driver.findElement(By.id("hplogo")) != null;
            isSearchBarPresent = _driver.findElement(
                    By.cssSelector("#tsf > div:last-child > div:first-child > div:nth-child(2)")) != null;
            isSearchButtonPresent = _driver.findElement(By.cssSelector("#tsf center > input:first-child")) != null;
            isGmailLinkPresent =
                    _driver.findElement(By.cssSelector(
                            "#viewport>div:nth-child(3)>div>div>div>div:first-child>div:first-child a")) != null;
        } finally {
            _driver.quit();
        }

        Assert.assertEquals(title, "Google");
        Assert.assertTrue(isLogoPresent);
        Assert.assertTrue(isSearchBarPresent);
        Assert.assertTrue(isSearchButtonPresent);
        Assert.assertTrue(isGmailLinkPresent);
    }

    @Test
    public void WikipediaTest() {
        boolean isCoatOfArmsPresent;
        boolean isPopulationTextPresent;
        boolean isAverageTempElPresent;
        boolean isTempInAprilTextPresent;
        boolean isCoronaVirusSubsectionPresent;
        boolean isPopulationDensityValuePresent;

        var coatOfArmsSelector =
                ".infobox > tbody > tr:nth-child(2) > td > table > tbody > tr:first-child > td:first-child > a";
        var populationTextSelector = ".infobox > tbody > tr:nth-child(15) td";
        var averageTempTextSelector = "#collapsibleTable0 > tbody > tr:nth-child(5) > th:last-child";
        var tempInAprilTextSelector = "#collapsibleTable0 > tbody > tr:nth-child(5) > th:nth-child(5)";
        var coronaVirusSubsectionSelector = "h3 span#Епідемія_коронавірусу";
        var populationDensityValueSelector = ".infobox > tbody > tr:nth-child(19) td";

        try {
            _driver.get("https://www.wikipedia.org/");
            new Select(_driver.findElement(By.id("searchLanguage"))).selectByValue("uk");
            _driver.findElement(By.id("searchInput")).sendKeys("Київ");
            _driver.findElement(By.cssSelector("button[type=\"submit\"]")).click();

            isCoatOfArmsPresent = _driver.findElement(By.cssSelector(coatOfArmsSelector)) != null;
            isPopulationTextPresent = _driver.findElement(By.cssSelector(populationTextSelector)) != null;
            isAverageTempElPresent = _driver.findElement(By.cssSelector(averageTempTextSelector)) != null;
            isTempInAprilTextPresent = _driver.findElement(By.cssSelector(tempInAprilTextSelector)) != null;
            isCoronaVirusSubsectionPresent = _driver.findElement(By.cssSelector(coronaVirusSubsectionSelector)) != null;
            isPopulationDensityValuePresent = _driver.findElement(By.cssSelector(populationDensityValueSelector)) != null;
        } finally {
            _driver.quit();
        }

        Assert.assertTrue(isCoatOfArmsPresent);
        Assert.assertTrue(isPopulationTextPresent);
        Assert.assertTrue(isAverageTempElPresent);
        Assert.assertTrue(isTempInAprilTextPresent);
        Assert.assertTrue(isCoronaVirusSubsectionPresent);
        Assert.assertTrue(isPopulationDensityValuePresent);
    }

    @Test
    public void Wikipedia__Kiev_Outstanding_Architectural_Monuments_Count__Is_20() {
        var monumentsListElementSelector = "ul:nth-of-type(11)";
        int actualMonumentsListSize = 0;
        try {
            //           https://uk.wikipedia.org/wiki/Київ
            _driver.get("https://uk.wikipedia.org/wiki/%D0%9A%D0%B8%D1%97%D0%B2");
            var monumentsElements =
                    _driver.findElement(By.cssSelector(monumentsListElementSelector))
                    .findElements(By.cssSelector("li"));

            actualMonumentsListSize = monumentsElements.size();

        }
        finally {
            _driver.quit();
        }

        Assert.assertEquals(20, actualMonumentsListSize);
    }
}
